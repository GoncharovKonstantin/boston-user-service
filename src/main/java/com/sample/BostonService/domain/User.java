package com.sample.BostonService.domain;

import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class User {

    @Id
    @Email
    private String email;

    @NotNull
    private String name;
    @NotNull
    private String surname;
    @NotNull
    private LocalDate birthday;
    @NotNull
    private String password;

    public User()
    {

    }

    public User(String email, String name, String surname, LocalDate birthday, String password) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password)
    {
        if (password == null)
            this.password = password;
        else
            this.password = DigestUtils.sha256Hex(password);

    }
}
