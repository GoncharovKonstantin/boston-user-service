package com.sample.BostonService.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class InvalidEmailException extends RuntimeException{
    public InvalidEmailException() {
        super("Invalid e-mail address");
    }
}
