package com.sample.BostonService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BostonServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BostonServiceApplication.class, args);
	}
}
